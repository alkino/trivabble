#!/usr/bin/env sh

# This is needed for compatibility with Safari 9, used in the iPad 2.
# This browser understands the code of Trivabble, except const and let.
# Let's sadly edit them out of javascript files on production to enable
# more people to play.

WORKING_DIRECTORY="$(dirname "$(dirname "$0")")"

if [ -d "$WORKING_DIRECTORY/.git" ]; then
    if [ -z "$(git status --porcelain | grep '.js$')" ]; then
        sed -i -E 's/(^|\(|\s)(let|const)(\s)/\1var\3/g' "$WORKING_DIRECTORY"/*.js
    else
        >&2 echo "You have untracked changes in JS files. I'm not going to mess up with your working copy."
	exit 1
    fi
fi
