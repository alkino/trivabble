#!/usr/bin/env sh

exec "$(dirname "$0")/disable-const-let.sh"

if ! [ -f "$(dirname "$(dirname "$0")")/config.js" ]; then
    touch "$(dirname "$(dirname "$0")")/config.js"
fi
